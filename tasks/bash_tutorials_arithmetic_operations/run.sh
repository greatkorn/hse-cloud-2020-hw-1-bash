#! /bin/bash
#export LC_NUMERIC="en_US.UTF-8"

read str
printf %.3f $(echo "$str" | bc -l)
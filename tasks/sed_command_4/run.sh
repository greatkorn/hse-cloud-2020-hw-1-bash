#! /bin/bash

rev | sed 's/\([0-9]\{4\}\)/****/2g' | rev
#we can also pipe 3 seds with limitation on 1st occurence, but i'm not sure what's better
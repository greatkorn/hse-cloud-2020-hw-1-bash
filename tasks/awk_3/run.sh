#! /bin/bash

awk '{ 
    sum = $2 + $3 + $4;
    grade = "FAIL";
    if (sum >= 240) grade = "A"; else
    if (sum >= 180) grade = "B"; else
    if (sum >= 150) grade = "C";
    print $0 " : " grade;
    }'